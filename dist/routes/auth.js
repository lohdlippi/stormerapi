"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const heroes_1 = require("../db/schemas/heroes");
const logger_1 = require("../logger");
const passport = require("passport");
const jwt = require("jsonwebtoken");
class AuthRouter {
    /**
     * Initialize the AuthRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     *
     * @memberOf AuthRouter
     */
    getAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const heroes = yield heroes_1.Heroes.find({});
                res.status(200).send({
                    status: 'success',
                    heroes: heroes
                });
            }
            catch (e) {
                logger_1.default.error(e);
                const message = e.message ? e.message : 'There was an error';
                res.status(500).send({
                    message: message
                });
            }
        });
    }
    login(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            passport.authenticate('twitter', { scope: ['include_email=true'] });
        });
    }
    callback(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            // create jwt from req.user
            // then redirect to site
            const secret = process.env.SECRET;
            const current_env = process.env.CURRENT_ENV;
            const token = jwt.sign({ data: req.user }, secret, { expiresIn: '6h' });
            res.redirect(`${current_env}?token=${token}`);
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/login', passport.authenticate('twitter', { scope: ['include_email=true'] }));
        this.router.get('/callback', passport.authenticate('twitter', { failureRedirect: '/login' }), this.callback);
    }
}
exports.AuthRouter = AuthRouter;
// Create the AuthRouter, and export its configured Express.Router
const AuthRoutes = new AuthRouter();
AuthRoutes.init();
exports.default = AuthRoutes.router;
