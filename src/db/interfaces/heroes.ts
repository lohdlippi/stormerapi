export interface IHero {
  name: string;
  posts: string[];
  twitterId: string;
  email: string;
  token: string;
  tokenSecret: string;
  username: string;
  photoUrl: string;
}
