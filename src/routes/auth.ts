import { Router, Request, Response, NextFunction } from 'express';
import { IHeroModel, Heroes } from '../db/schemas/heroes';
import logger from '../logger';
import * as passport from 'passport';
import * as jwt from 'jsonwebtoken';
import {
  idSchema,
  validateParams,
  heroSchema,
  validateBody
} from '../helpers/validation';

export class AuthRouter {
  router: Router;

  /**
   * Initialize the AuthRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * GET all Heroes.
   * 
   * @param {Request} req 
   * @param {Response} res 
   * @param {NextFunction} next 
   * 
   * @memberOf AuthRouter
   */
  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const heroes = await Heroes.find({});
      res.status(200).send({
        status: 'success',
        heroes: heroes
      });
    } catch (e) {
      logger.error(e);
      const message = e.message ? e.message : 'There was an error';
      res.status(500).send({
        message: message
      });
    }
  }

  async login(req: Request, res: Response, next: NextFunction) {
    passport.authenticate('twitter', { scope: ['include_email=true'] });
  }
  async callback(req: Request, res: Response, next: NextFunction) {
    // create jwt from req.user
    // then redirect to site
    const secret = process.env.SECRET;
    const current_env = process.env.CURRENT_ENV;
    const token = jwt.sign({ data: req.user }, secret, { expiresIn: '6h' });
    res.redirect(`${current_env}?token=${token}`);
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get(
      '/login',
      passport.authenticate('twitter', { scope: ['include_email=true'] })
    );
    this.router.get(
      '/callback',
      passport.authenticate('twitter', { failureRedirect: '/login' }),
      this.callback
    );
  }
}

// Create the AuthRouter, and export its configured Express.Router
const AuthRoutes = new AuthRouter();
AuthRoutes.init();

export default AuthRoutes.router;
