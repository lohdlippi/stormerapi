"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const logger_1 = require("../../logger");
exports.heroSchema = new mongoose_1.Schema({
    email: { type: String },
    twitterId: { type: String, unique: true },
    posts: Array,
    name: String,
    token: String,
    tokenSecret: String,
    username: String,
    photoUrl: String
}, { timestamps: true });
const handleE11000 = function (error, res, next) {
    if (error.email === 'MongoError' && error.code === 11000) {
        logger_1.default.error(error);
        next(new Error('There was a duplicate key error'));
    }
    else {
        next();
    }
};
exports.heroSchema.post('save', handleE11000);
exports.heroSchema.post('update', handleE11000);
exports.heroSchema.post('findOneAndUpdate', handleE11000);
exports.heroSchema.post('insertMany', handleE11000);
exports.Heroes = mongoose_1.model('Heroes', exports.heroSchema);
