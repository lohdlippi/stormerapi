import { Router, Request, Response, NextFunction } from 'express';
import { IHeroModel, Heroes } from '../db/schemas/heroes';
import logger from '../logger';
import * as passport from 'passport';
import * as jwt from 'jsonwebtoken';
import {
  idSchema,
  validateParams,
  heroSchema,
  validateBody
} from '../helpers/validation';
// import * as Twit from 'twit'
var Twit = require('twitter');

const verifyToken = () => {
  return (req, res, next) => {
    let token;
    const secret = process.env.SECRET;
    if (req.headers && req.headers.authorization) {
      var parts = req.headers.authorization.split(' ');
      if (parts.length == 2) {
        var scheme = parts[0],
          credentials = parts[1];

        if (/^Bearer$/i.test(scheme)) {
          token = credentials;
        }
      } else {
        res.status(401).send({
          message: 'Format is Authorization: Bearer [token]'
        });
      }
    } else if (req.param('token')) {
      token = req.param('token');
      delete req.query.token;
    } else {
      res.status(401).send({
        message: 'No authorization header was found'
      });
    }

    jwt.verify(token, secret, (err, decoded) => {
      if (err || !decoded) {
        res.redirect('/auth/login');
      }
      Heroes.findOne({ twitterId: decoded.data.twitterId }).then(function(
        hero
      ) {
        req.current_user = hero;
        next();
      });
    });
  };
};

export class StormerRouter {
  router: Router;

  /**
   * Initialize the StormerRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * GET all Heroes.
   * 
   * @param {Request} req 
   * @param {Response} res 
   * @param {NextFunction} next 
   * 
   * @memberOf StormerRouter
   * 
   */
  async testWrite(req, res, next) {
    const arr = [1, 2, 3, 4, 5];
    for (const item in arr) {
      logger.info(item);
    }
    res.end();
  }
  async postTweets(req: any, res: Response, next: NextFunction) {
    try {
      const postData: any = req.body.postData;
      const interval = req.body.interval ? req.body.interval : 100;

      const user = req.current_user;
      const consumerKey = process.env.TWITTER_CONSUMER_KEY;
      const consumerSecret = process.env.TWITTER_CONSUMER_SECRET;
      var T = new Twit({
        consumer_key: consumerKey,
        consumer_secret: consumerSecret,
        access_token_key: user.token,
        access_token_secret: user.tokenSecret,
        timeout_ms: 60 * 10000 // optional HTTP request timeout to apply to all requests.
      });

      function delay(t) {
        return new Promise(function(resolve, reject) {
          setTimeout(resolve, t);
        });
      }

      let replyToId;
      // if images.. upload images then post tweets
      for (const item in postData) {
        if (postData.hasOwnProperty(item)) {
          let tweetObject = postData[item];
          let mediaList;
          let params;
          if (tweetObject.images) {
            for (let image of tweetObject.images.images) {
              image = image.split(',')[1];
              const media = await T.post('media/upload', { media_data: image });
              if (mediaList) {
                mediaList = mediaList + `,${media.media_id_string}`;
              } else {
                mediaList = media.media_id_string;
              }
            }
          }

          params = {
            status: tweetObject.tweet,
            in_reply_to_status_id: replyToId ? replyToId : null
          };
          if (mediaList) {
            params.media_ids = mediaList;
          }

          const post = await T.post('statuses/update', params);
          res.write(item);
          await delay(interval);
          replyToId = post.id_str;
        }
      }

      res.end();
    } catch (e) {
      logger.error(e);
      const message = e.message ? e.message : 'There was an error';
      res.status(500).send({
        message: message
      });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.post('/', verifyToken(), this.postTweets);
    this.router.get('/test', this.testWrite);
    // this.router.get('/callback', passport.authenticate('twitter', { failureRedirect: '/login' }), this.callback)
  }
}

// Create the StormerRouter, and export its configured Express.Router
const StormerRoutes = new StormerRouter();
StormerRoutes.init();

export default StormerRoutes.router;
