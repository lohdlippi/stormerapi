"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const session = require("express-session");
const logger_1 = require("./logger");
const HeroRouter_1 = require("./routes/HeroRouter");
const auth_1 = require("./routes/auth");
const stormer_1 = require("./routes/stormer");
const heroes_1 = require("./db/schemas/heroes");
const passport = require("passport");
const auth_2 = require("./helpers/auth");
passport.use(auth_2.twitterStrategy); // twitter login
passport.serializeUser(function (hero, done) {
    done(null, hero.id);
});
passport.deserializeUser(function (id, done) {
    heroes_1.Heroes.findById(id, function (err, user) {
        done(err, user);
    });
});
const whitelist = [
    'http://localhost:8080',
    'https://stormerapp.co',
    'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop'
];
var corsOptions = {
    origin: function (origin, callback) {
        logger_1.default.info(`Origin ${origin}`);
        if (whitelist.indexOf(origin) !== -1 || origin == undefined) {
            callback(null, true);
        }
        else {
            callback(new Error('Not allowed by CORS'));
        }
    },
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
// Creates and configures an ExpressJS web server.
class App {
    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.database();
        this.routes();
    }
    // Configure Express middleware.
    middleware() {
        this.express.use(morgan('dev', { stream: logger_1.default.stream })); // pipe morgan outputs through logger
        this.express.use(bodyParser.json({ limit: '500mb' }));
        this.express.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));
        this.express.use(passport.initialize());
        this.express.use(session({
            secret: 'keyboard cat',
            resave: false,
            saveUninitialized: true
        }));
        this.express.use(passport.session());
    }
    //configure database connection
    database() {
        const MONGODB_CONNECTION = process.env.MONGODB_URI;
        //use q promises
        global.Promise = require('q').Promise;
        mongoose.Promise = global.Promise;
        mongoose.connect(MONGODB_CONNECTION);
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function () {
            // we're connected!
            console.log('db connected');
        });
    }
    // Configure API endpoints.
    routes() {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        let router = express.Router();
        // enable cors
        router.all('*', cors(corsOptions));
        // placeholder route handler
        router.get('/', (req, res, next) => {
            res.json({
                message: 'Hello World!'
            });
        });
        this.express.use('/', router);
        this.express.use('/auth', auth_1.default);
        this.express.use('/v1/stormer', stormer_1.default);
        this.express.use('/v1/heroes', HeroRouter_1.default);
    }
}
exports.default = new App().express;
