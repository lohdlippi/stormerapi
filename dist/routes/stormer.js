"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const heroes_1 = require("../db/schemas/heroes");
const logger_1 = require("../logger");
const jwt = require("jsonwebtoken");
// import * as Twit from 'twit'
var Twit = require('twitter');
const verifyToken = () => {
    return (req, res, next) => {
        let token;
        const secret = process.env.SECRET;
        if (req.headers && req.headers.authorization) {
            var parts = req.headers.authorization.split(' ');
            if (parts.length == 2) {
                var scheme = parts[0], credentials = parts[1];
                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                }
            }
            else {
                res.status(401).send({
                    message: 'Format is Authorization: Bearer [token]'
                });
            }
        }
        else if (req.param('token')) {
            token = req.param('token');
            delete req.query.token;
        }
        else {
            res.status(401).send({
                message: 'No authorization header was found'
            });
        }
        jwt.verify(token, secret, (err, decoded) => {
            if (err || !decoded) {
                res.redirect('/auth/login');
            }
            heroes_1.Heroes.findOne({ twitterId: decoded.data.twitterId }).then(function (hero) {
                req.current_user = hero;
                next();
            });
        });
    };
};
class StormerRouter {
    /**
     * Initialize the StormerRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     *
     * @memberOf StormerRouter
     *
     */
    testWrite(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const arr = [1, 2, 3, 4, 5];
            for (const item in arr) {
                logger_1.default.info(item);
            }
            res.end();
        });
    }
    postTweets(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const postData = req.body.postData;
                const interval = req.body.interval ? req.body.interval : 100;
                const user = req.current_user;
                const consumerKey = process.env.TWITTER_CONSUMER_KEY;
                const consumerSecret = process.env.TWITTER_CONSUMER_SECRET;
                var T = new Twit({
                    consumer_key: consumerKey,
                    consumer_secret: consumerSecret,
                    access_token_key: user.token,
                    access_token_secret: user.tokenSecret,
                    timeout_ms: 60 * 10000 // optional HTTP request timeout to apply to all requests.
                });
                function delay(t) {
                    return new Promise(function (resolve, reject) {
                        setTimeout(resolve, t);
                    });
                }
                let replyToId;
                // if images.. upload images then post tweets
                for (const item in postData) {
                    if (postData.hasOwnProperty(item)) {
                        let tweetObject = postData[item];
                        let mediaList;
                        let params;
                        if (tweetObject.images) {
                            for (let image of tweetObject.images.images) {
                                image = image.split(',')[1];
                                const media = yield T.post('media/upload', { media_data: image });
                                if (mediaList) {
                                    mediaList = mediaList + `,${media.media_id_string}`;
                                }
                                else {
                                    mediaList = media.media_id_string;
                                }
                            }
                        }
                        params = {
                            status: tweetObject.tweet,
                            in_reply_to_status_id: replyToId ? replyToId : null
                        };
                        if (mediaList) {
                            params.media_ids = mediaList;
                        }
                        const post = yield T.post('statuses/update', params);
                        res.write(item);
                        yield delay(interval);
                        replyToId = post.id_str;
                    }
                }
                res.end();
            }
            catch (e) {
                logger_1.default.error(e);
                const message = e.message ? e.message : 'There was an error';
                res.status(500).send({
                    message: message
                });
            }
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', verifyToken(), this.postTweets);
        this.router.get('/test', this.testWrite);
        // this.router.get('/callback', passport.authenticate('twitter', { failureRedirect: '/login' }), this.callback)
    }
}
exports.StormerRouter = StormerRouter;
// Create the StormerRouter, and export its configured Express.Router
const StormerRoutes = new StormerRouter();
StormerRoutes.init();
exports.default = StormerRoutes.router;
