"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const heroes_1 = require("../db/schemas/heroes");
const logger_1 = require("../logger");
const validation_1 = require("../helpers/validation");
class HeroRouter {
    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all Heroes.
     *
     * @param {Request} req
     * @param {Response} res
     * @param {NextFunction} next
     *
     * @memberOf HeroRouter
     */
    getAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const heroes = yield heroes_1.Heroes.find({});
                res.status(200).send({
                    status: 'success',
                    heroes: heroes
                });
            }
            catch (e) {
                logger_1.default.error(e);
                const message = e.message ? e.message : 'There was an error';
                res.status(500).send({
                    message: message
                });
            }
        });
    }
    /**
     * Create one
     */
    createOne(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const hero = new heroes_1.Heroes(req.value.body);
                const newHero = yield hero.save();
                res.status(200).send({
                    message: 'success',
                    hero: newHero
                });
            }
            catch (e) {
                logger_1.default.error(e);
                const message = e.message ? e.message : 'There was an error';
                res.status(500).send({
                    message: message
                });
            }
        });
    }
    /**
     * GET one hero by id
     */
    getOne(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.value.params;
                const hero = yield heroes_1.Heroes.findById(id);
                if (hero) {
                    res.status(200).send({
                        message: 'success',
                        hero: hero
                    });
                }
                else {
                    res.status(404).send({
                        message: 'No hero found with the given id.',
                        status: res.status
                    });
                }
            }
            catch (e) {
                logger_1.default.error(e);
                const message = e.message ? e.message : 'There was an error';
                res.status(500).send({
                    message: message
                });
            }
        });
    }
    removeOne(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.value.params;
                const hero = yield heroes_1.Heroes.remove({ _id: id });
                if (hero) {
                    res.status(200).send({
                        message: 'success'
                    });
                }
                else {
                    res.status(404).send({
                        message: 'No hero found with the given id.',
                        status: res.status
                    });
                }
            }
            catch (e) {
                logger_1.default.error(e);
                const message = e.message ? e.message : 'There was an error';
                res.status(500).send({
                    message: message
                });
            }
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.getAll);
        this.router.get('/:id', validation_1.validateParams(validation_1.idSchema, 'id'), this.getOne);
        this.router.post('/', validation_1.validateBody(validation_1.heroSchema), this.createOne);
        this.router.delete('/:id', validation_1.validateParams(validation_1.idSchema, 'id'), this.removeOne);
    }
}
exports.HeroRouter = HeroRouter;
// Create the HeroRouter, and export its configured Express.Router
const heroRoutes = new HeroRouter();
heroRoutes.init();
exports.default = heroRoutes.router;
