const TwitterStrategy = require('passport-twitter').Strategy;
import { IHeroModel, Heroes } from '../db/schemas/heroes';

export const twitterStrategy = new TwitterStrategy(
  {
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    callbackURL: process.env.TWITTER_CALLBACK
  },
  (token, tokenSecret, profile, done) => {
    var me = new Heroes({
      // email: profile.emails[0].value ? profile.emails[0].value: null,
      name: profile.displayName,
      username: profile.username,
      twitterId: profile.id,
      token: token,
      tokenSecret: tokenSecret,
      photoUrl: profile.photos[0].value
    });
    /* save if new */
    Heroes.findOne({ twitterId: me.twitterId }, function(err, u) {
      if (!u) {
        me.save(function(err, user) {
          if (err) return done(err);
          done(null, user);
        });
      } else {
        const updateObj = {
          // email: profile.emails[0].value ? profile.emails[0].value: null,
          name: profile.displayName,
          username: profile.username,
          twitterId: profile.id,
          token: token,
          tokenSecret: tokenSecret,
          photoUrl: profile.photos[0].value
        };
        Heroes.findOneAndUpdate(
          { twitterId: me.twitterId },
          updateObj,
          function(err, user) {
            if (err) return done(err);
            done(null, user);
          }
        );
      }
    });
  }
);
