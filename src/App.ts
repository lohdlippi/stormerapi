import * as path from 'path';
import * as express from 'express';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import mongoose = require('mongoose');
import * as cors from 'cors';
import * as session from 'express-session';
import logger from './logger';
import HeroRouter from './routes/HeroRouter';
import authRouter from './routes/auth';
import stormerRouter from './routes/stormer';
import { IHeroModel, Heroes } from './db/schemas/heroes';

import * as passport from 'passport';
import { twitterStrategy } from './helpers/auth';
passport.use(twitterStrategy); // twitter login

passport.serializeUser(function(hero: IHeroModel, done) {
  done(null, hero.id);
});

passport.deserializeUser(function(id, done) {
  Heroes.findById(id, function(err, user) {
    done(err, user);
  });
});

const whitelist = [
  'http://localhost:8080',
  'https://stormerapp.co',
  'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop'
];
var corsOptions = {
  origin: function(origin, callback) {
    logger.info(`Origin ${origin}`);
    if (whitelist.indexOf(origin) !== -1 || origin == undefined) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

// Creates and configures an ExpressJS web server.
class App {
  // ref to Express instance
  public express: express.Application;

  //Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.middleware();
    this.database();
    this.routes();
  }

  // Configure Express middleware.
  private middleware(): void {
    this.express.use(morgan('dev', { stream: logger.stream })); // pipe morgan outputs through logger
    this.express.use(bodyParser.json({ limit: '500mb' }));
    this.express.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));
    this.express.use(passport.initialize());
    this.express.use(
      session({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true
      })
    );

    this.express.use(passport.session());
  }

  //configure database connection
  private database(): void {
    const MONGODB_CONNECTION: string = process.env.MONGODB_URI;
    //use q promises
    global.Promise = require('q').Promise;
    mongoose.Promise = global.Promise;
    mongoose.connect(MONGODB_CONNECTION);
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      // we're connected!
      console.log('db connected');
    });
  }

  // Configure API endpoints.
  private routes(): void {
    /* This is just to get up and running, and to make sure what we've got is
     * working so far. This function will change when we start to add more
     * API endpoints */
    let router = express.Router();
    // enable cors
    router.all('*', cors(corsOptions));
    // placeholder route handler
    router.get('/', (req, res, next) => {
      res.json({
        message: 'Hello World!'
      });
    });
    this.express.use('/', router);
    this.express.use('/auth', authRouter);
    this.express.use('/v1/stormer', stormerRouter);
    this.express.use('/v1/heroes', HeroRouter);
  }
}

export default new App().express;
